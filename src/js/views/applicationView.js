import $ from 'jquery';
import _ from 'underscore';
import * as Backbone from 'backbone';
import List from '../collections/itemsList';
import ItemView from '../views/item/item';
import controlsTemplate from '../templates/controls/controls.html';
import Common from '../common';

export default Backbone.View.extend({

    el: '#task-manager',
    template: _.template(controlsTemplate),

    events: {
        'click .add-task-btn': 'createTask',
        'keypress #new-task': 'createOnEnter',
        'click #clear-completed': 'clearCompleted',
        'click #toggle-all': 'toggleAllComplete'
    },

    initialize: function () {
        this.allCheckbox = this.$('#toggle-all')[0];
        this.$taskDescription = this.$('#new-task');
        this.$taskTitle = this.$('#new-task-title');
        this.$priority = this.$('#priority option:selected');
        this.$deadDate = this.$('#dead-date');
        this.$deadHour = this.$('#dead-hour option:selected');
        this.$deadMinutes = this.$('#dead-min option:selected');
        this.$filterPanel = $('#filter-panel');
        this.$main = this.$('#main');
        this.$todoList = $('#task-list');

        this.listenTo(List, 'add', this.addOne);
        this.listenTo(List, 'reset', this.addAll);
        this.listenTo(List, 'change:completed', this.filterOne);
        this.listenTo(List, 'filter', this.filterAll);
        this.listenTo(List, 'all', _.debounce(this.render, 0));

        List.fetch({reset: true});
    },

    render: function () {
        var completed = List.completed().length;
        var remaining = List.remaining().length;

        if (List.length) {
            this.$main.show();
            this.$filterPanel.show();

            this.$filterPanel.html(this.template({
                completed: completed,
                remaining: remaining
            }));

            this.$('#filters li a')
                .removeClass('selected')
                .filter('[href="#/' + (Common.TodoFilter || '') + '"]')
                .addClass('selected');
        } else {
            this.$main.hide();
            this.$filterPanel.hide();
        }

        this.allCheckbox.checked = !remaining;
    },

    addOne: function (todo) {
        var view = new ItemView({model: todo});
        this.$todoList.append(view.render().el);
    },

    addAll: function () {
        this.$todoList.empty();
        List.each(this.addOne, this);
    },

    filterOne: function (todo) {
        todo.trigger('visible');
    },

    filterAll: function () {
        List.each(this.filterOne, this);
    },

    newAttributes: function () {
        let date = new Date(),
            newDate = date.getMonth() +1 + '.' + date.getDate() + '.'+ date.getFullYear()
                + ' ' + date.getHours() + ":" + date.getMinutes();

        return {
            title: this.$taskTitle.val().trim(),
            description: this.$taskDescription.val().trim(),
            order: List.nextOrder(),
            completed: false,
            priority: this.$priority.text(),
            createdAt: newDate,
            deadLine: this.$deadDate.val() + ' ' + this.$deadHour.text() + ':' + this.$deadMinutes.text()
        };
    },

    createTask: function (e) {
        if (!this.$taskDescription.val().trim()) {
            return;
        }
        this.createNewTask();
    },

    createOnEnter: function (e) {
        if (e.which !== Common.ENTER_KEY || !this.$taskDescription.val().trim()) {
            return;
        }
        this.createNewTask();
    },

    createNewTask: function() {
        List.create(this.newAttributes());
        this.$taskDescription.val('');
        this.$taskTitle.val('');
    },

    clearCompleted: function () {
        _.invoke(List.completed(), 'destroy');
        return false;
    },

    toggleAllComplete: function () {
        var completed = this.allCheckbox.checked;

        List.each(function (todo) {
            todo.save({
                completed: completed
            });
        });
    }
});
